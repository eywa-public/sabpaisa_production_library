package com.loki.android.n

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.loki.android.n.R
import com.sabpaisa.gateway.android.sdk.SabPaisaGateway
import com.sabpaisa.gateway.android.sdk.interfaces.IPaymentSuccessCallBack
import com.sabpaisa.gateway.android.sdk.models.TransactionResponsesModel

class MainActivity : AppCompatActivity(), IPaymentSuccessCallBack<TransactionResponsesModel> {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        SabPaisaGateway.Companion.sync(this)
    }

    fun openSDK(view: View) {
        val sabPaisaGateway1 =
            SabPaisaGateway.builder()
                .setAmount(350.0)   //Mandatory Parameter
                .setFirstName("First Name Sab") //Mandatory Parameter
                .setLastName("Last Sab") //Mandatory Parameter
                .setMobileNumber("1234556644") //Mandatory Parameter
                .setEmailId("123@123.com")//Mandatory Parameter
                .setClientCode("TM001")
                .setSabPaisaPaymentScreen(true)
                .setClientTransactionId("0123456789")
                .setAesApiIv("YN2v8qQcU3rGfA1y")
                .setAesApiKey("kaY9AIhuJZNvKGp2")
                .setTransUserName("rajiv.moti_336")
                .setTransUserPassword("RIADA_SP336")
                .build()

        SabPaisaGateway.setInitUrl("https://securepay.sabpaisa.in/SabPaisa/sabPaisaInit?v=1")
        SabPaisaGateway.setEndPointBaseUrl("https://securepay.sabpaisa.in")
        SabPaisaGateway.setTxnEnquiryEndpoint("https://txnenquiry.sabpaisa.in")

        sabPaisaGateway1.init(this@MainActivity,this)
    }

    override fun onPaymentFail(message: TransactionResponsesModel?) {
        Log.d("SABPAISA", "Payment Fail")
//        resultCode?.text = "Payment Success: ${message?.status}"
//        resultCode?.setTextColor(ContextCompat.getColor(this, R.color.red))
        findViewById<TextView>(R.id.status).text="Payment status (${message?.status} ${message?.clientTxnId})"
    }

    override fun onPaymentSuccess(response: TransactionResponsesModel?) {
        Log.d("SABPAISA", "Payment Success${response?.statusCode}")
//        resultCode?.text = "Payment Success: ${response?.status}"
//        resultCode?.setTextColor(ContextCompat.getColor(this, R.color.material_color_green_600))
        findViewById<TextView>(R.id.status).text="Payment status (${response?.status} ${response?.clientTxnId})"
    }


}