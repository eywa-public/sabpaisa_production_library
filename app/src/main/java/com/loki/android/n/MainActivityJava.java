package com.loki.android.n;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.loki.android.n.R;
import com.sabpaisa.gateway.android.sdk.SabPaisaGateway;
import com.sabpaisa.gateway.android.sdk.interfaces.IPaymentSuccessCallBack;
import com.sabpaisa.gateway.android.sdk.models.TransactionResponsesModel;


public class MainActivityJava extends AppCompatActivity implements IPaymentSuccessCallBack<TransactionResponsesModel> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SabPaisaGateway.Companion.syncImages(this);
    }


    public void openSDK(View view) {
        SabPaisaGateway sabPaisaGateway1 =
                SabPaisaGateway.Companion.builder()
                        .setAmount(350.0)   //Mandatory Parameter
                        .setFirstName("First Name Sab") //Mandatory Parameter
                        .setLastName("Last Sab") //Mandatory Parameter
                        .setMobileNumber("1234556644") //Mandatory Parameter
                        .setEmailId("123@123.com")//Mandatory Parameter
                        .setClientCode("LPSD1")
                        .setSabPaisaPaymentScreen(true)
                        .setAesApiIv("1234567890123456")
                        .setAesApiKey("QVMtR1JBUy1QUk9E")
                        .setTransUserName("Abh789@sp")
                        .setTransUserPassword("P8c3WQ7ei")
                        .build();

        SabPaisaGateway.Companion.setInitUrlSabpaisa("https://stage-securepay.sabpaisa.in/SabPaisa/sabPaisaInit?v=1");
        SabPaisaGateway.Companion.setEndPointBaseUrlSabpaisa("https://stage-securepay.sabpaisa.in");
        SabPaisaGateway.Companion.setTxnEnquiryEndpointSabpaisa("https://stage-txnenquiry.sabpaisa.in");

        sabPaisaGateway1.init(this, this);
    }


    @Override
    public void onPaymentFail(@Nullable TransactionResponsesModel transactionResponsesModel) {
        Log.d("SABPAISA", "Payment Fail");
//        resultCode?.text = "Payment Success: ${message?.status}"
//        resultCode?.setTextColor(ContextCompat.getColor(this, R.color.red))
        ((TextView) findViewById(R.id.status)).setText("Payment status (" + transactionResponsesModel.getStatus() + " " + transactionResponsesModel.getClientTxnId());
    }

    @Override
    public void onPaymentSuccess(@Nullable TransactionResponsesModel transactionResponsesModel) {
        Log.d("SABPAISA", "Payment Success");
//        resultCode?.text = "Payment Success: ${response?.status}"
//        resultCode?.setTextColor(ContextCompat.getColor(this, R.color.material_color_green_600))
        ((TextView) findViewById(R.id.status)).setText("Payment status (" + transactionResponsesModel.getStatus() + " " + transactionResponsesModel.getClientTxnId());

    }
}
