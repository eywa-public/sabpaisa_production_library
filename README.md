
# SabPaisa Android SDK

SabPaisa SDK is the simplest way to integrate payment gateway in your existing application.
SDK provides different payment modes:



## Payment Modes

- Credit Card
- Debit Card
- Rupay Credit Card
- Rupay Debit Card
- Net Banking
- QR Code Scanner
- UPI Payment
- EMI
- NEFT/RTGS Challan Generation

AAR URL : https://gitlab.com/eywa-public/sabpaisa_production_library/-/blob/main/app/libs/gatewayAndroid-release.aar

## Import
You can copy aar from this project and then put it in your "libs" folder.
Add this line in Gradle

```
    implementation files('./libs/gatewayAndroid-release.aar')
    implementation 'com.google.android.material:material:1.5.0-alpha02'
    implementation 'androidx.constraintlayout:constraintlayout:2.1.4'
    implementation 'com.airbnb.android:lottie:3.4.0'
    implementation 'androidx.navigation:navigation-fragment-ktx:2.5.3'
    implementation 'androidx.navigation:navigation-ui-ktx:2.5.3'
    implementation 'com.squareup.picasso:picasso:2.5.2'
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.6.1'
    implementation 'com.google.code.gson:gson:2.9.1'
    implementation 'com.squareup.okhttp3:okhttp:4.9.2'
```
![Sample Image](./S1.png "Sample Image")

## Usage/Examples

If you are calling this sdk from activity then implement Our SDK Interface
```
IPaymentSuccessCallBack<TransactionResponsesModel>
```

Like this:

#### Kotlin:
```
class MainActivity : AppCompatActivity(),IPaymentSuccessCallBack<TransactionResponsesModel>{

 override fun onPaymentFail(message: String?) {
        SabPaisaLogsTag.genericLogsError("Payment Fail")
    }

    override fun onPaymentSuccess(response: TransactionResponsesModel?) {
        SabPaisaLogsTag.genericLogs("Payment Success${response?.pgRespCode}")
    }
}
```

#### Java:
```
class MainActivity extends AppCompatActivity() implements IPaymentSuccessCallBack<TransactionResponsesModel>{

@Override
    public void onPaymentSuccess(@Nullable TransactionResponsesModel response) {
        
    }

    @Override
    public void onPaymentFail(@Nullable String message) {

    }
}

```
To call sdk you need to initalize SabPaisaGateway class. Below is the code you can use by calling a function or from a button click listener.

#### Kotlin:
```Kotlin

val sabPaisaGateway1 =
            SabPaisaGateway.builder()
                .setAmount("100")   //Mandatory Parameter
                .setFirstName("first name") //Mandatory Parameter
                .setLastName("last name") //Mandatory Parameter
                .setMobileNumber("4343434343") //Mandatory Parameter
                .setEmailId("test@gmail.com")//Mandatory Parameter
                .setSabPaisaPaymentScreen(false)
                .setSalutation(SabPaisaGateway.EMPTY_STRING)
                .setClientCode("TM001")
                .setAesApiIv("YN2v8qQcU3rGfA1y")
                .setAesApiKey("kaY9AIhuJZNvKGp2")
                .setTransUserName("rajiv.moti_336")
                .setTransUserPassword("RIADA_SP336")
                .setCurrency(currency?.text.toString())
                .setCallbackUrl("https://www.your-callback-url.com") // optional
                .setClientTransactionId("random but unique transaction")
                .build()
            SabPaisaGateway.initUrl = "https://sdkstaging.sabpaisa.in/SabPaisa/sabPaisaInit?v=1"
            SabPaisaGateway.endPointBaseUrl = "https://sdkstaging.sabpaisa.in"
            SabPaisaGateway.txnEnquiryEndpoint = "https://stage-txnenquiry.sabpaisa.in"
            sabPaisaGateway1.init(this@MainActivity, this)
```

#### JAVA:
```C++

        String clientTransactionId = sampleClientTransactionIdGenerationFunction(); // you can create 20 digit clientTransactionId
        SabPaisaGateway sabPaisaGateway1 =
                SabPaisaGateway.Companion.builder()
                        .setAmount(599)   //Mandatory Parameter
                        .setFirstName("test") //Mandatory Parameter
                        .setLastName("123") //Mandatory Parameter
                        .setMobileNumber("5454322334") //Mandatory Parameter
                        .setEmailId("asdasd@gmail.com")//Mandatory Parameter
                        .setClientCode("TM001")
                        .setAesApiIv("YN2v8qQcU3rGfA1y")
                        .setAesApiKey("kaY9AIhuJZNvKGp2")
                        .setTransUserName("rajiv.moti_336")
                        .setTransUserPassword("RIADA_SP336")
                        .setClientTransactionId(clientTransactionId)
                        .build();
        SabPaisaGateway.Companion.setInitUrl("https://sdkstaging.sabpaisa.in/SabPaisa/sabPaisaInit?v=1");
        SabPaisaGateway.Companion.setEndPointBaseUrl("https://sdkstaging.sabpaisa.in");
        SabPaisaGateway.Companion.setTxnEnquiryEndpoint("https://stage-txnenquiry.sabpaisa.in");

        sabPaisaGateway1.init(this, this);

```

